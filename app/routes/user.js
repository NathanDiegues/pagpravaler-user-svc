// ./routes/user.js
const Router = require('express-promise-router');
var uniqid = require('uniqid')

const db = require('../db');

const router = new Router();

module.exports = router;

router.get('/:cpf', async (req, res) => {
    const { cpf } = req.params;
    const { rows } = await db.query(
        "SELECT u.*, d.boleto_valor, d.renda_mensal, d.foto_rg, "+
        "d.foto_endereco, d.created_at as info_created_at, d.updated_at as info_updated_at, d.aprovado "+
        "FROM pgprv_usuario u "+
        "left outer join pgprv_usr_dados d on u.id = d.id where u.cpf = $1", [cpf]);
    res.send(rows[0]);
})

//registrar usuário
router.post('/register', async (req, res) => {
    const id = uniqid();
    const cpf  = req.body.cpf;
    const nome  = req.body.nome;
    const tel  = req.body.tel;
    const email  = req.body.email;
    const senha  = req.body.senha;
    const created_at = new Date();

    const { query } = await db.query(
        "INSERT INTO pgprv_usuario(id, cpf, nome, telefone, "+
        "email, password, created_at) VALUES ($1, $2, $3, $4, $5, $6, $7)",
        [id, cpf, nome, tel, email, senha, created_at], (err, result) => {
            if (err) {
                res.status(500).send(err);
                return console.error('Error executing query', err.stack);
            }
        })

    res.status(200).send(id);
})

//update nas informações do usuário
router.post('/update', async (req, res) => {
    const id = req.body.id;
    const cpf  = req.body.cpf;
    const nome  = req.body.nome;
    const tel  = req.body.tel;
    const email  = req.body.email;
    const senha  = req.body.senha;
    const updated_at = new Date();

    const { query } = await db.query(
        "update pgprv_usuario set cpf = $2, nome = $3, "+
        "telefone = $4, email = $5, password = $6, updated_at = $7 "+
	    "where id = $1",
        [id, cpf, nome, tel, email, senha, updated_at], (err, result) => {
            if (err) {
                res.status(500).send(err);
                return console.error('Error executing query', err.stack);
            }
        })

    res.status(200).send("Usuário alterado com sucesso.");
})

//adicionar informações extras
router.post('/addInfo', async (req, res) => {
    const id = req.body.id;
    const boleto = req.body.boleto;
    const renda = req.body.renda;
    const foto_rg = req.body.foto_rg;
    const foto_endereco = req.body.foto_endereco;
    const created_at = new Date();

    const { query } = await db.query(
        "INSERT INTO pgprv_usr_dados(id, boleto_valor, renda_mensal, "+
        "foto_rg, foto_endereco, created_at) "+
        "VALUES ($1, $2, $3, $4, $5, $6)",
        [id, boleto, renda, foto_rg, foto_endereco, created_at], (err, result) => {
            if (err) {
                res.status(500).send(err);
                return console.error('Error executing query', err.stack);
            }
        })

    res.status(200).send("Informações adicionais registradas com sucesso.");
})

//atualiza informações extras
router.post('/updateInfo', async (req, res) => {
    const id = req.body.id;
    const boleto = req.body.boleto;
    const renda = req.body.renda;
    const foto_rg = req.body.foto_rg;
    const foto_endereco = req.body.foto_endereco;
    const updated_at = new Date();

    const { query } = await db.query(
        "UPDATE pgprv_usr_dados SET boleto_valor = $2, renda_mensal = $3, "+
        "foto_rg = $4, foto_endereco = $5, updated_at = $6 "+
        "WHERE id = $1",
        [id, boleto, renda, foto_rg, foto_endereco, updated_at], (err, result) => {
            if (err) {
                res.status(500).send(err);
                return console.error('Error executing query', err.stack);
            }
        })

    res.status(200).send("Informações adicionais alteradas com sucesso.");
})

//aprovar usuário
router.get('/:id/approve', async (req, res) => {
    const id  = req.params;

    const { query } = await db.query(
        "UPDATE pgprv_usr_dados SET aprovado = true "+
        "WHERE id = $1",
        [id], (err, result) => {
            if (err) {
                res.status(500).send(err);
                return console.error('Error executing query', err.stack);
            }
        })

    res.status(200).send("Usuário aprovado.");
})

//não aprovar usuário
router.get('/:id/disapprove', async (req, res) => {
    const id  = req.params;

    const { query } = await db.query(
        "UPDATE pgprv_usr_dados SET aprovado = false "+
        "WHERE id = $1",
        [id], (err, result) => {
            if (err) {
                res.status(500).send(err);
                return console.error('Error executing query', err.stack);
            }
        })

    res.status(200).send("Usuário reprovado.");
})
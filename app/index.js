// ./index.js
const express = require('express');
const mountRoutes = require('./routes');

const app = express();

const port = 8080;

//para processar formulários
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//incia os endpoints
mountRoutes(app);

app.get('/', (req, res) => {
    res.send('Microservice User OK')
});

// app.get('/test', async (req, res) => {
//     const { rows } = await db.query("SELECT * FROM test");
//     res.send(rows);
// })

app.post('/test', (req, res) => {

    var keke = req.body;
    // var keke = req.body.email;

    res.send(keke);
})

app.listen(port, () => console.log(`User listening on port ${port}`));